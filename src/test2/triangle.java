package test2;

public class triangle extends Shape implements TwoDimensionalShapeInterface {
	double base;
	double height;

	public triangle(String color, String shape, double base, double height) {
		super(color, shape);
		// TODO Auto-generated constructor stub
		this.base = base;
		this.height = height;
	}

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public void calculateArea() {
		// TODO Auto-generated method stub
		super.calculateArea();
		double area = 0.5 * base * height;

	}

	@Override
	public void printInfo() {
		// TODO Auto-generated method stub
		super.printInfo();

		System.out.println(	 "The selected Shape is: "+ this.shape+ "The color of the shape : " + this.color + "\n" + "The base of the shape :" + this.base
				+ " \n" + "The height of the shape :" + this.height);
	}
}
