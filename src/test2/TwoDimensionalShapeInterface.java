package test2;

public interface TwoDimensionalShapeInterface {
	public void calculateArea();
	public void printInfo();
}
