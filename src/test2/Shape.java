package test2;

import java.util.ArrayList;

public class Shape implements TwoDimensionalShapeInterface {
	String color;
	String shape;
	ArrayList<Shape> shapesList = new ArrayList<Shape>();

	public ArrayList<Shape> getShapesList() {
		return shapesList;
	}

	public void setShapesList(ArrayList<Shape> shapesList) {
		this.shapesList = shapesList;
	}

	public Shape(String color, String shape) {
		this.color = color;
		this.shape = shape;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getShape() {
		return shape;
	}

	public void setShape(String shape) {
		this.shape = shape;
	}

	public void shapesList(Shape s) {
		shapesList.add(s);
	}

	@Override
	public void calculateArea() {
		// TODO Auto-generated method stub

	}

	@Override
	public void printInfo() {
		
		// TODO Auto-generated method stub
	

	}

}
