package test2;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		int choice = 0;

		while (choice != 4) {
			// 1. show the menu
			showMenu();

			// 2. get the user input
			System.out.println("Enter a number: ");
			choice = keyboard.nextInt();

			// 3. DEBUG: Output what the user typed in
			System.out.println("You entered: " + choice);
			System.out.println();
			if (choice == 1) {

				Shape s1 = new Shape("blue", "Triangle");
				triangle T1 = new triangle("blue", "Triangle", 5, 2);
				s1.shapesList(T1);
				s1.calculateArea();
				s1.printInfo();

			}
			if (choice == 2) {

				Shape s1 = new Shape("black", "Square");
				Square T2 = new Square("black", "Square", 5);
				s1.shapesList(T2);
				s1.calculateArea();
				s1.printInfo();

			}
			if (choice == 3) {

		break;

			}

		}
		
	}

	public static void showMenu() {
		System.out.println("AREA GENERATOR");
		System.out.println("==============");
		System.out.println("1. Triangle");
		System.out.println("2. Square");
		System.out.println("3. Exit");
	}

}
